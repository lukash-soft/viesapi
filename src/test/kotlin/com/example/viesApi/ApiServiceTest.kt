import com.example.viesApi.ApiFetchException
import com.example.viesApi.ApiService
import eu.viesapi.client.VIESData
import eu.viesapi.client.VIESAPIClient
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import reactor.test.StepVerifier
import java.net.URI

@ExtendWith(SpringExtension::class)
@ActiveProfiles("test")
@TestPropertySource(locations = ["classpath:application-test.properties"])
class ApiServiceTest {

    private lateinit var apiService: ApiService
    private lateinit var viesApiClient: VIESAPIClient

    @Value("\${test_api.url}")
    private lateinit var url: String

    @Value("\${test_api.key}")
    private lateinit var key: String

    @Value("\${test_api.id}")
    private lateinit var id: String

    @BeforeEach
    fun setUp() {
        viesApiClient = VIESAPIClient(id, key).apply { setURL(URI.create(url).toURL()) }
        apiService = ApiService(viesApiClient)
    }


    @Test
    fun `fetchPersonByNip should return ApiModel`() {
        // given
        val validNip = "PL7272445205"

        // when
        val result = apiService.fetchPersonByNip(validNip)

        // then
        assertThat(result.vatNumber).isEqualTo("7272445205")
        assertThat(result.countryCode).isEqualTo("PL")
        assertThat(result.traderName).isEqualTo("ROBERT JAZGARA")
    }

    @Test
    fun `fetchPersonByNip should return exception due to null values`(){
        // given
        val invalidNip = "PL7272445201"

        //when, then
        assertThatThrownBy { apiService.fetchPersonByNip(invalidNip) }
                .isInstanceOf(ApiFetchException::class.java)
                .hasMessage("Failed to fetch person due to null values")
    }
}

