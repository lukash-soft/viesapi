package com.example.viesApi

import eu.viesapi.client.VIESAPIClient
import org.springframework.stereotype.Service


@Service
class ApiService(private val viesApiClient: VIESAPIClient) {
    fun fetchPersonByNip(nip: String): ApiModel {
        try{
            val viesData = viesApiClient.getVIESData(nip)
            return ApiModel(
                    viesData.countryCode,
                    viesData.vatNumber,
                    viesData.traderName,
                    viesData.traderCompanyType,
                    viesData.traderAddress,
                    viesData.isValid,
            )
        }
        catch (e:NullPointerException){
            throw ApiFetchException("Failed to fetch person due to null values", e)
        }catch (e: Exception){
            throw ApiFetchException("Failed to fetch person due to ${e.message}", e)
        }

    }
}
