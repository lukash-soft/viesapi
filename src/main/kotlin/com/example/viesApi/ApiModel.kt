package com.example.viesApi

class ApiModel(
        var countryCode: String,
        var vatNumber: String,
        var traderName: String,
        var traderCompanyName: String,
        var traderAddress: String,
        var isValid: Boolean)
