package com.example.viesApi

class ApiFetchException (message: String, cause: Throwable) : Exception(message, cause)
