package com.example.viesApi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ViesApiApplication

fun main(args: Array<String>) {
	runApplication<ViesApiApplication>(*args)
}
