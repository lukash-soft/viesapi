package com.example.viesApi

import eu.viesapi.client.VIESAPIClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.net.URI

@Configuration
class ViesApiClientConfig {

    @Value("\${test_api.url}")
    private lateinit var url: String
    @Value("\${test_api.key}")
    private lateinit var key: String
    @Value("\${test_api.id}")
    private lateinit var id: String

    @Bean
    fun viesApiClient(): VIESAPIClient {
        return VIESAPIClient(id, key).apply {
            setURL(URI.create(url).toURL())
        }
    }
}
