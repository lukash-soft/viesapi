package com.example.viesApi

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController("api")
class ApiController(private val viesService: ApiService) {
    @GetMapping("/vies/{nip}")
    fun getPersonByNip(@PathVariable nip: String): ApiModel {
        return viesService.fetchPersonByNip(nip)
    }

}
